RM		= rm -f
BINARY		= gateway
IMAGE_NAME	= weepi/${BINARY}
PKG 		:= "gitlab.com/weepi/$(BINARY)"

$(BINARY):
	go build -v $(PKG)

install:
	go install -v

dep:
	rm -rf Godeps vendor && godep save

image:
	docker build -t $(IMAGE_NAME) .

all: $(BINARY)

clean:
	$(RM) $(BINARY)

re: clean all

.PHONY: $(BINARY) all re
