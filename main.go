package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	accountPb "gitlab.com/weepi/account/proto"
	authPb "gitlab.com/weepi/auth/proto"
	"gitlab.com/weepi/common/utils"
	eventPb "gitlab.com/weepi/event/proto"
	sportsPb "gitlab.com/weepi/sports/proto"

	"github.com/caarlos0/env"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type config struct {
	ListenAddr           string `env:"LISTEN_ADDR,required"`
	WeepiAuthEndpoint    string `env:"WEEPI_AUTH_ENDPOINT"`
	WeepiAccountEndpoint string `env:"WEEPI_ACCOUNT_ENDPOINT"`
	WeepiEventEndpoint   string `env:"WEEPI_EVENT_ENDPOINT"`
	WeepiSportsEndpoint  string `env:"WEEPI_SPORTS_ENDPOINT"`
	UseTLS               bool   `env:"USE_TLS"`
	TLSCrt               string `env:"TLS_CRT"`
	TLSKey               string `env:"TLS_KEY"`
}

const (
	configFilename = "config"
	tagName        = "env"
)

var (
	Logger *logrus.Logger
	Cfg    config
)

func init() {
	var helpFlag = flag.Bool("help", false, "display help")
	flag.Parse()
	if *helpFlag {
		help()
		os.Exit(0)
	}

	logrus.SetFormatter(&logrus.TextFormatter{
		TimestampFormat: "2006-01-02T15:04:05.000",
		FullTimestamp:   true,
	})
	Logger = logrus.New()
}

func help() {
	fmt.Println("The following env vars can be set:")
	for _, v := range utils.GetStructTags(Cfg, tagName) {
		fmt.Printf("%s\n", v)
	}
}

func main() {
	Logger.Infof("service started at %s", time.Now())
	runtime.HTTPError = customHTTPError

	// Load env vars from file
	if err := godotenv.Load(configFilename); err != nil {
		Logger.WithError(err).Warn("could not read configuration from file")
	}
	// Get config from env vars
	if err := env.Parse(&Cfg); err != nil {
		Logger.WithError(err).Fatal("could not parse configuration")
	}

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}

	var err error
	// Registering weepis
	err = authPb.RegisterWeepiAuthHandlerFromEndpoint(ctx, mux, Cfg.WeepiAuthEndpoint, opts)
	if err != nil {
		Logger.WithError(err).Fatal("registering weepi auth handler")
	}
	err = accountPb.RegisterWeepiAccountHandlerFromEndpoint(ctx, mux, Cfg.WeepiAccountEndpoint, opts)
	if err != nil {
		Logger.WithError(err).Fatal("registering weepi account handler")
	}
	err = eventPb.RegisterWeepiEventHandlerFromEndpoint(ctx, mux, Cfg.WeepiEventEndpoint, opts)
	if err != nil {
		Logger.WithError(err).Fatal("registering weepi event handler")
	}
	err = sportsPb.RegisterWeepiSportsHandlerFromEndpoint(ctx, mux, Cfg.WeepiSportsEndpoint, opts)
	if err != nil {
		Logger.WithError(err).Fatal("registering weepi sports handler")
	}

	// Add CORS
	m := allowCORS(mux)

	if Cfg.UseTLS {
		if err := http.ListenAndServeTLS(Cfg.ListenAddr, Cfg.TLSCrt, Cfg.TLSKey, m); err != nil {
			Logger.WithError(err).Fatal("serving https")
		}
		return
	}
	if err := http.ListenAndServe(Cfg.ListenAddr, m); err != nil {
		Logger.WithError(err).Fatal("serving http")
	}
}
