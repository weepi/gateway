# Weepi gateway !

The weepi gateway microservice was used by [Weepen](https://weepen.io/) to manage gRPC to JSON proxy.

The docker compose file in the [deploy repository](https://gitlab.com/weepi/deploy) was used to manage it.

