// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto/auth.proto

/*
Package auth is a generated protocol buffer package.

It is generated from these files:
	proto/auth.proto

It has these top-level messages:
	Creds
	Secret
	Uuid
	Token
	Auth
*/
package auth

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import _ "google.golang.org/genproto/googleapis/api/annotations"
import google_protobuf1 "github.com/golang/protobuf/ptypes/empty"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Type int32

const (
	Type_REGULAR Type = 0
	Type_COACH   Type = 1
	Type_PREMIUM Type = 2
	Type_STAFF   Type = 3
)

var Type_name = map[int32]string{
	0: "REGULAR",
	1: "COACH",
	2: "PREMIUM",
	3: "STAFF",
}
var Type_value = map[string]int32{
	"REGULAR": 0,
	"COACH":   1,
	"PREMIUM": 2,
	"STAFF":   3,
}

func (x Type) String() string {
	return proto.EnumName(Type_name, int32(x))
}
func (Type) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

type AuthType int32

const (
	AuthType_STANDARD AuthType = 0
	AuthType_FACEBOOK AuthType = 1
	AuthType_GOOGLE   AuthType = 2
)

var AuthType_name = map[int32]string{
	0: "STANDARD",
	1: "FACEBOOK",
	2: "GOOGLE",
}
var AuthType_value = map[string]int32{
	"STANDARD": 0,
	"FACEBOOK": 1,
	"GOOGLE":   2,
}

func (x AuthType) String() string {
	return proto.EnumName(AuthType_name, int32(x))
}
func (AuthType) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

type Creds struct {
	Uuid     *Uuid    `protobuf:"bytes,1,opt,name=uuid" json:"uuid,omitempty"`
	Secret   *Secret  `protobuf:"bytes,2,opt,name=secret" json:"secret,omitempty"`
	AuthType AuthType `protobuf:"varint,3,opt,name=auth_type,json=authType,enum=AuthType" json:"auth_type,omitempty"`
}

func (m *Creds) Reset()                    { *m = Creds{} }
func (m *Creds) String() string            { return proto.CompactTextString(m) }
func (*Creds) ProtoMessage()               {}
func (*Creds) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *Creds) GetUuid() *Uuid {
	if m != nil {
		return m.Uuid
	}
	return nil
}

func (m *Creds) GetSecret() *Secret {
	if m != nil {
		return m.Secret
	}
	return nil
}

func (m *Creds) GetAuthType() AuthType {
	if m != nil {
		return m.AuthType
	}
	return AuthType_STANDARD
}

type Secret struct {
	Value string `protobuf:"bytes,1,opt,name=value" json:"value,omitempty"`
}

func (m *Secret) Reset()                    { *m = Secret{} }
func (m *Secret) String() string            { return proto.CompactTextString(m) }
func (*Secret) ProtoMessage()               {}
func (*Secret) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *Secret) GetValue() string {
	if m != nil {
		return m.Value
	}
	return ""
}

type Uuid struct {
	Value string `protobuf:"bytes,1,opt,name=value" json:"value,omitempty"`
}

func (m *Uuid) Reset()                    { *m = Uuid{} }
func (m *Uuid) String() string            { return proto.CompactTextString(m) }
func (*Uuid) ProtoMessage()               {}
func (*Uuid) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *Uuid) GetValue() string {
	if m != nil {
		return m.Value
	}
	return ""
}

type Token struct {
	Value string `protobuf:"bytes,1,opt,name=value" json:"value,omitempty"`
}

func (m *Token) Reset()                    { *m = Token{} }
func (m *Token) String() string            { return proto.CompactTextString(m) }
func (*Token) ProtoMessage()               {}
func (*Token) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{3} }

func (m *Token) GetValue() string {
	if m != nil {
		return m.Value
	}
	return ""
}

type Auth struct {
	IsAuth bool  `protobuf:"varint,1,opt,name=is_auth,json=isAuth" json:"is_auth,omitempty"`
	Uuid   *Uuid `protobuf:"bytes,2,opt,name=uuid" json:"uuid,omitempty"`
}

func (m *Auth) Reset()                    { *m = Auth{} }
func (m *Auth) String() string            { return proto.CompactTextString(m) }
func (*Auth) ProtoMessage()               {}
func (*Auth) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{4} }

func (m *Auth) GetIsAuth() bool {
	if m != nil {
		return m.IsAuth
	}
	return false
}

func (m *Auth) GetUuid() *Uuid {
	if m != nil {
		return m.Uuid
	}
	return nil
}

func init() {
	proto.RegisterType((*Creds)(nil), "Creds")
	proto.RegisterType((*Secret)(nil), "Secret")
	proto.RegisterType((*Uuid)(nil), "Uuid")
	proto.RegisterType((*Token)(nil), "Token")
	proto.RegisterType((*Auth)(nil), "Auth")
	proto.RegisterEnum("Type", Type_name, Type_value)
	proto.RegisterEnum("AuthType", AuthType_name, AuthType_value)
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for WeepiAuth service

type WeepiAuthClient interface {
	InsertCreds(ctx context.Context, in *Creds, opts ...grpc.CallOption) (*google_protobuf1.Empty, error)
	UpdateCreds(ctx context.Context, in *Creds, opts ...grpc.CallOption) (*google_protobuf1.Empty, error)
	GetToken(ctx context.Context, in *Creds, opts ...grpc.CallOption) (*Token, error)
	IsAuth(ctx context.Context, in *google_protobuf1.Empty, opts ...grpc.CallOption) (*Auth, error)
	DeleteCreds(ctx context.Context, in *google_protobuf1.Empty, opts ...grpc.CallOption) (*google_protobuf1.Empty, error)
}

type weepiAuthClient struct {
	cc *grpc.ClientConn
}

func NewWeepiAuthClient(cc *grpc.ClientConn) WeepiAuthClient {
	return &weepiAuthClient{cc}
}

func (c *weepiAuthClient) InsertCreds(ctx context.Context, in *Creds, opts ...grpc.CallOption) (*google_protobuf1.Empty, error) {
	out := new(google_protobuf1.Empty)
	err := grpc.Invoke(ctx, "/WeepiAuth/InsertCreds", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *weepiAuthClient) UpdateCreds(ctx context.Context, in *Creds, opts ...grpc.CallOption) (*google_protobuf1.Empty, error) {
	out := new(google_protobuf1.Empty)
	err := grpc.Invoke(ctx, "/WeepiAuth/UpdateCreds", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *weepiAuthClient) GetToken(ctx context.Context, in *Creds, opts ...grpc.CallOption) (*Token, error) {
	out := new(Token)
	err := grpc.Invoke(ctx, "/WeepiAuth/GetToken", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *weepiAuthClient) IsAuth(ctx context.Context, in *google_protobuf1.Empty, opts ...grpc.CallOption) (*Auth, error) {
	out := new(Auth)
	err := grpc.Invoke(ctx, "/WeepiAuth/IsAuth", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *weepiAuthClient) DeleteCreds(ctx context.Context, in *google_protobuf1.Empty, opts ...grpc.CallOption) (*google_protobuf1.Empty, error) {
	out := new(google_protobuf1.Empty)
	err := grpc.Invoke(ctx, "/WeepiAuth/DeleteCreds", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for WeepiAuth service

type WeepiAuthServer interface {
	InsertCreds(context.Context, *Creds) (*google_protobuf1.Empty, error)
	UpdateCreds(context.Context, *Creds) (*google_protobuf1.Empty, error)
	GetToken(context.Context, *Creds) (*Token, error)
	IsAuth(context.Context, *google_protobuf1.Empty) (*Auth, error)
	DeleteCreds(context.Context, *google_protobuf1.Empty) (*google_protobuf1.Empty, error)
}

func RegisterWeepiAuthServer(s *grpc.Server, srv WeepiAuthServer) {
	s.RegisterService(&_WeepiAuth_serviceDesc, srv)
}

func _WeepiAuth_InsertCreds_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Creds)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WeepiAuthServer).InsertCreds(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/WeepiAuth/InsertCreds",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WeepiAuthServer).InsertCreds(ctx, req.(*Creds))
	}
	return interceptor(ctx, in, info, handler)
}

func _WeepiAuth_UpdateCreds_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Creds)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WeepiAuthServer).UpdateCreds(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/WeepiAuth/UpdateCreds",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WeepiAuthServer).UpdateCreds(ctx, req.(*Creds))
	}
	return interceptor(ctx, in, info, handler)
}

func _WeepiAuth_GetToken_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Creds)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WeepiAuthServer).GetToken(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/WeepiAuth/GetToken",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WeepiAuthServer).GetToken(ctx, req.(*Creds))
	}
	return interceptor(ctx, in, info, handler)
}

func _WeepiAuth_IsAuth_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(google_protobuf1.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WeepiAuthServer).IsAuth(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/WeepiAuth/IsAuth",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WeepiAuthServer).IsAuth(ctx, req.(*google_protobuf1.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _WeepiAuth_DeleteCreds_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(google_protobuf1.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WeepiAuthServer).DeleteCreds(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/WeepiAuth/DeleteCreds",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WeepiAuthServer).DeleteCreds(ctx, req.(*google_protobuf1.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

var _WeepiAuth_serviceDesc = grpc.ServiceDesc{
	ServiceName: "WeepiAuth",
	HandlerType: (*WeepiAuthServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "InsertCreds",
			Handler:    _WeepiAuth_InsertCreds_Handler,
		},
		{
			MethodName: "UpdateCreds",
			Handler:    _WeepiAuth_UpdateCreds_Handler,
		},
		{
			MethodName: "GetToken",
			Handler:    _WeepiAuth_GetToken_Handler,
		},
		{
			MethodName: "IsAuth",
			Handler:    _WeepiAuth_IsAuth_Handler,
		},
		{
			MethodName: "DeleteCreds",
			Handler:    _WeepiAuth_DeleteCreds_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "proto/auth.proto",
}

func init() { proto.RegisterFile("proto/auth.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 456 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x52, 0xc1, 0x6e, 0xd3, 0x40,
	0x10, 0x8d, 0xdd, 0x78, 0x63, 0x8f, 0xa1, 0x5a, 0x6d, 0x11, 0xa4, 0xa1, 0x40, 0xe5, 0x03, 0xaa,
	0x72, 0x58, 0x8b, 0x20, 0x90, 0xe8, 0xcd, 0x4d, 0x9c, 0x10, 0xd1, 0x62, 0xb4, 0x49, 0xc4, 0xb1,
	0x72, 0xc9, 0xd2, 0x5a, 0x0d, 0xb6, 0x15, 0xaf, 0x2b, 0xe5, 0xca, 0x2f, 0xf0, 0x49, 0x7c, 0x02,
	0xbf, 0xc0, 0x87, 0xa0, 0x1d, 0xdb, 0xbd, 0x20, 0x4b, 0x3d, 0x79, 0xe7, 0x3d, 0xbf, 0x79, 0x33,
	0xfb, 0x16, 0x68, 0xbe, 0xcd, 0x54, 0xe6, 0xc7, 0xa5, 0xba, 0xe1, 0x78, 0x1c, 0x1c, 0x5d, 0x67,
	0xd9, 0xf5, 0x46, 0xfa, 0x71, 0x9e, 0xf8, 0x71, 0x9a, 0x66, 0x2a, 0x56, 0x49, 0x96, 0x16, 0x35,
	0xfb, 0xbc, 0x66, 0xb1, 0xba, 0x2a, 0xbf, 0xfb, 0xf2, 0x47, 0xae, 0x76, 0x15, 0xe9, 0xdd, 0x82,
	0x35, 0xde, 0xca, 0x75, 0xc1, 0x0e, 0xa1, 0x5b, 0x96, 0xc9, 0xba, 0x6f, 0x1c, 0x1b, 0x27, 0xee,
	0xc8, 0xe2, 0xab, 0x32, 0x59, 0x0b, 0x84, 0xd8, 0x2b, 0x20, 0x85, 0xfc, 0xb6, 0x95, 0xaa, 0x6f,
	0x22, 0xd9, 0xe3, 0x0b, 0x2c, 0x45, 0x0d, 0xb3, 0xd7, 0xe0, 0xe8, 0x69, 0x2e, 0xd5, 0x2e, 0x97,
	0xfd, 0xbd, 0x63, 0xe3, 0x64, 0x7f, 0xe4, 0xf0, 0xa0, 0x54, 0x37, 0xcb, 0x5d, 0x2e, 0x85, 0x1d,
	0xd7, 0x27, 0xef, 0x25, 0x90, 0x4a, 0xc9, 0x9e, 0x80, 0x75, 0x17, 0x6f, 0x4a, 0x89, 0x76, 0x8e,
	0xa8, 0x0a, 0xef, 0x08, 0xba, 0xda, 0xb6, 0x85, 0x7d, 0x01, 0xd6, 0x32, 0xbb, 0x95, 0x69, 0x0b,
	0x7d, 0x0a, 0x5d, 0x6d, 0xc9, 0x9e, 0x41, 0x2f, 0x29, 0x2e, 0xb5, 0x27, 0xf2, 0xb6, 0x20, 0x49,
	0x81, 0x44, 0xb3, 0xa1, 0xf9, 0xdf, 0x86, 0xc3, 0xf7, 0xd0, 0xd5, 0x03, 0x32, 0x17, 0x7a, 0x22,
	0x9c, 0xad, 0xce, 0x03, 0x41, 0x3b, 0xcc, 0x01, 0x6b, 0x1c, 0x05, 0xe3, 0x8f, 0xd4, 0xd0, 0xf8,
	0x17, 0x11, 0x5e, 0xcc, 0x57, 0x17, 0xd4, 0xd4, 0xf8, 0x62, 0x19, 0x4c, 0xa7, 0x74, 0x6f, 0x38,
	0x02, 0xbb, 0x59, 0x93, 0x3d, 0x02, 0x7b, 0xb1, 0x0c, 0x3e, 0x4f, 0x02, 0x31, 0xa1, 0x1d, 0x5d,
	0x4d, 0x83, 0x71, 0x78, 0x16, 0x45, 0x9f, 0xa8, 0xc1, 0x00, 0xc8, 0x2c, 0x8a, 0x66, 0xe7, 0x21,
	0x35, 0x47, 0xbf, 0x4d, 0x70, 0xbe, 0x4a, 0x99, 0x27, 0x38, 0x94, 0x0f, 0xee, 0x3c, 0x2d, 0xe4,
	0x56, 0x55, 0x29, 0x10, 0x8e, 0xdf, 0xc1, 0x53, 0x5e, 0x85, 0xc6, 0x9b, 0xd0, 0x78, 0xa8, 0x43,
	0xf3, 0x3a, 0xec, 0x0c, 0xdc, 0x55, 0xbe, 0x8e, 0x95, 0x7c, 0x98, 0xe0, 0xe0, 0xe7, 0x9f, 0xbf,
	0xbf, 0xcc, 0xc7, 0x03, 0xdb, 0xbf, 0x7b, 0x83, 0x4f, 0xe6, 0xd4, 0x18, 0xb2, 0x77, 0x60, 0xcf,
	0xa4, 0xaa, 0x2e, 0xb3, 0x69, 0x40, 0x38, 0xd6, 0xde, 0x21, 0x0a, 0x0e, 0xbc, 0xfd, 0x46, 0xe0,
	0x2b, 0x8d, 0x6b, 0xd9, 0x07, 0x20, 0xf3, 0xea, 0x2a, 0x5b, 0xdc, 0x06, 0x16, 0xa6, 0xee, 0x51,
	0xec, 0x01, 0xec, 0xde, 0x94, 0x45, 0xe0, 0x4e, 0xe4, 0x46, 0x36, 0x53, 0xb7, 0xe9, 0xdb, 0xb6,
	0xa8, 0x1b, 0x0e, 0xef, 0x1b, 0x5e, 0x11, 0xfc, 0xe3, 0xed, 0xbf, 0x00, 0x00, 0x00, 0xff, 0xff,
	0xf2, 0x12, 0xc7, 0xc9, 0x0d, 0x03, 0x00, 0x00,
}
